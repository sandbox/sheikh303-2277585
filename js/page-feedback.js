/**
 * @file
 * This file contains the ajax call to store feedback and other front-end functionality
 */
var feedback_missing = Drupal.t("Please select an option");
var feedback_success = Drupal.t("Thank you for your feedback");
var feedback_other_missing = Drupal.t("Please write your comment");
var is_no_clicked = false;
(function($) {
    Drupal.behaviors.page_feedback = {
        attach: function(context, settings) {
            $('#feedback-choice-yes', context).click(function() {
                if (is_no_clicked) {
                    return false;
                }
                page_feedback_submit(1, "");
                return false;
            });
            $('#feedback-choice-no', context).click(function() {
                is_no_clicked = true;
                $('#feedback-improve-tips').slideDown(400);
            });
            $("#feedbackSubmit", context).click(function() {
                remarks = false;
                $("#feedback-missing").hide();
                radio_val = $('input:radio[name=remarks]:checked').val();
                if (radio_val == "open-ended") {
                    remarks = $("#feedbackTextArea").val();
                    if (!remarks) {
                        $("#page-feedback-msg-no").html(feedback_other_missing).addClass('alert-warning').slideDown("slow");
                        window.setTimeout(function() {
                            $("#page-feedback-msg-no").slideUp();
                        }, 2500);
                        return false;
                    }
                }
                else {
                    remarks = radio_val;
                }
                if (!remarks) {
                    $("#page-feedback-remarks-err").html(feedback_missing).slideDown("slow");
                    window.setTimeout(function() {
                        $("#page-feedback-submit-msg").slideUp();
                    }, 2500);
                    return false;

                }
                page_feedback_submit(0, remarks);
            });
        }
    };
    function page_feedback_submit(value, remarks) {
        $.ajax({
            url: '/page-feedback/ajax/submit',
            data: {value: value, page_uri: document.URL, remark: remarks},
            type: "POST",
            success: function(data) {
                if (data == "Success") {
                    $("#page-feedback-wrap").hide(500);
                    $("#page-feedback-submit-msg").html(feedback_success);
                }
                else {
                    return false;
                }
            }
        });
    }
})(jQuery);
