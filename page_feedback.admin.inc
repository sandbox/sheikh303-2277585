<?php

/**
 * @file
 * This file contains all the admin functionality.
 */

/**
 * Admin Section.
 */
function page_feedback_admin_listing() {
  $header = array(
    array('data' => t('Page'), 'field' => 'pf.page_uri'),
    array('data' => t('Useful'), 'field' => 'pf.feedback_val'),
    array('data' => t('Remarks'), 'field' => 'pf.feedback_remarks'),
    array('data' => t('Submitted'), 'field' => 'pf.submitted'),
  );
  try {
    $page_feedbacks = db_select("page_feedback", 'pf')
        ->fields('pf', array(
          'page_uri', 'feedback_val',
          'feedback_remarks', 'submitted',
            )
        )
        ->extend('TableSort')
        ->orderByHeader($header)
        ->orderBy('pf.submitted', 'DESC')
        ->extend('PagerDefault')->limit(20)
        ->execute();
  }
  catch (PDOException $e) {
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }
  $rows = array();
  foreach ($page_feedbacks as $row) {

    $row->page_uri = l($row->page_uri, $row->page_uri);
    $row->feedback_val = ($row->feedback_val) ? "Yes" : "No";
    $rows[] = array('data' => (array) $row);
  }
  $output = theme('table', array(
        "header" => $header,
        "rows" => $rows,
        "attributes" => array(),
        "sticky" => TRUE,
        "caption" => l(t("Export"), "admin/config/gp-tools/page-feedback/list/export") . '  |  ' . l(t("Clear"), "admin/config/gp-tools/page-feedback/list/clear", array('attributes' => array('onclick' => 'return confirm("Are You Sure?"); '))),
        "colgroups" => array(),
        "empty" => t("No Feedback Available."),
          )
      ) . theme("pager");
  return $output;
}

/**
 * Export feedback record function callback.
 *
 * @global string $base_url
 */
function page_feedback_admin_export() {
  global $base_url;
  header("Content-type:application/vnd.ms-excel;name='excel'");
  header("Content-Disposition: attachment; filename=Page Feedback-" . date("d-m-Y_h_i_a") . ".xls");
  header("Pragma: no-cache");
  header("Expires: 0");
  $header = array('Page URI', 'Useful', 'Remarks', 'Submitted');
  try {
    $page_feedbacks = db_select("page_feedback", 'pf')
        ->fields('pf', array(
          'page_uri', 'feedback_val',
          'feedback_remarks', 'submitted',
            )
        )
        ->orderBy('pf.submitted', 'DESC')
        ->execute();
  }
  catch (PDOException $e) {
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }
  $rows = array();
  foreach ($page_feedbacks as $row) {
    $row->page_uri = l($base_url . $row->page_uri, $base_url . $row->page_uri);
    $row->feedback_val = ($row->feedback_val) ? "Yes" : "No";
    $rows[] = array('data' => (array) $row);
  }
  $output = theme('table', array(
    "header" => $header,
    "rows" => $rows,
    "attributes" => array(),
    "empty" => t("No Feedback Available."),
    "sticky" => FALSE,
    "caption" => "",
    "colgroups" => array(),
      )
  );
  echo $output;
  drupal_exit();
}

/**
 * Clear feedback record function callback.
 */
function page_feedback_admin_clear() {
  try {
    db_truncate('page_feedback')->execute();
  }
  catch (PDOException $e) {
    drupal_set_message(t('Error: %message', array('%message' => $e->getMessage())), 'error');
  }

  drupal_set_message(t("Cleared page feedack data."));
  drupal_goto("admin/config/content-management/page-feedback/list");
}

/**
 * Admin setting form callback.
 *
 * @return array
 *   $form
 */
function page_feedback_admin_settings($form, $form_state) {
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t("Add options for negative remarks"),
  );
  $form['options']['new'] = array(
    '#type' => 'textfield',
    '#title' => t('Text'),
  );
  $form['options']['existing_options'] = array(
    '#title' => 'Existing Options',
    '#type' => 'fieldset',
    '#description' => t('Edit or write off any of them to remove'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $existing_options = unserialize(check_plain(variable_get('page_feedback_options', "")));
  if ($existing_options && count($existing_options)) {
    foreach ($existing_options as $key => $option) {
      $form['options']['existing_options'][$key] = array(
        '#type' => 'textfield',
        '#default_value' => check_plain($option),
      );
    }
  }
  $form['options']['has_open_ended'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide an open ended remark option'),
    '#default_value' => (variable_get('page_feedback_has_open_ended')) ? 1 : 0,
  );
  $form['options']['Save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Admin settings form submission function.
 */
function page_feedback_admin_settings_submit($form, &$form_state) {

  if ($form_state['values']['has_open_ended']) {
    variable_set('page_feedback_has_open_ended', TRUE);
  }
  else {
    variable_set('page_feedback_has_open_ended', FALSE);
  }
  $optins = array();
  if (!empty($form_state['values']['existing_options'])) {
    foreach ($form_state['values']['existing_options'] as $value) {
      if ($value != "") {
        $optins[] = filter_xss(check_plain($value));
      }
    }
  }
  if (!empty($form_state['values']['new'])) {
    $optins[] = filter_xss(check_plain($form_state['values']['new']));
  }
  variable_set('page_feedback_options', filter_xss(check_plain(serialize($optins))));
  drupal_set_message(t("Settings Stored"));
}
