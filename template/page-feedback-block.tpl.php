<?php
/**
 * @file
 * Template file to generate Page Feedback Block content.
 *
 * Available variables:
 * - $options: contains all the option given from admin panel
 *
 * System Variable
 * page_feedback_has_open_ended : to enable/disable open ended remarks field
 *
 * @ingroup themeable
 */
?>
<div id="page-feedback" class="clearfix">
  <div id="page-feedback-wrap">
    <div id="page-feedback-section">
      <label class="feedback-title"><?php echo t("Was this page helpful?"); ?></label>
      <div class="feedback-button">
        <button type="button" class="btn btn-custom btn-feedback-choice" id="feedback-choice-yes"><?php echo t("Yes"); ?></button>
        <button type="button" class="btn btn-custom btn-feedback-choice" id="feedback-choice-no"><?php echo t("No"); ?></button>    
      </div>
    </div>
    <div id="feedback-improve-tips" style="display: none;">    

      <h4><?php echo t("Help to improve"); ?></h4>
      <?php if(count($options)):?>
      <?php foreach ($options as $key => $option): ?>
        <div class="radio"><label for="radio-<?php echo $key ?>"><input type="radio" id="radio-<?php echo $key ?>" name="remarks" value="<?php echo $option ?>"><?php echo t("@option", array("@option" => $option)); ?></div>
          <?php endforeach; ?>
      <?php endif;?>    
          <?php if (variable_get('page_feedback_has_open_ended')): ?>
            <div class="radio"><label for="radio-open-ended"><input type="radio" id="radio-open-ended" name="remarks" value="open-ended" data-enhance="false"><?php echo t("Other : Please Specify"); ?></label></div>
            <div class="form-group"><textarea id="feedbackTextArea" name="feedbackText" class="tell-us-more form-control" data-enhance="false"></textarea></div>
          <?php endif; ?>      
          <input id="page-url" type="hidden" value="<?php echo request_uri(); ?>">                    
          <button type="button" id="feedbackSubmit" class="btn-page-feedback-submit"><?php echo t("Submit"); ?></button>
          <div class="clearfix clear"></div>
          <div id="page-feedback-remarks-err"></div>
      </div>
    </div>
    <div id="page-feedback-submit-msg"></div>
  </div>
