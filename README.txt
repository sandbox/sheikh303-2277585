This module provided a block to be used to collect 
feedback on page's content from the site visitor.
It will store the feedback with respective 
page link, which can be exported into CSV file on demand.

Key Features:
1.A feedback collection block. Can be assigned at any theme region
2.Dynamic option adding for remarks on negative feedback
3.Ajax based feedback Submission
4.Export on demand
5.Clear record on demand

Admin Panel/Configuration Page Link: 
admin/config/content-management/page-feedback/settings

List, Export, Clear Page: 
admin/config/content-management/page-feedback/list 

Block Title:
"Page Feedback"
